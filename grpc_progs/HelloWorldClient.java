/*
 * Copyright 2015 The gRPC Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.grpc.examples.helloworld;

import java.util.Iterator;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
// import io.grpc.internal.Stream;
import io.grpc.stub.StreamObserver;
import io.grpc.examples.helloworld.GreeterGrpc.GreeterStub;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.graalvm.compiler.hotspot.stubs.Stub;

/**
 * A simple client that requests a greeting from the {@link HelloWorldServer}.
 */
public class HelloWorldClient {
  private static final Logger logger = Logger.getLogger(HelloWorldClient.class.getName());

  private final GreeterGrpc.GreeterBlockingStub blockingStub;
  private final GreeterGrpc.GreeterStub asyncStub;

  /** Construct client for accessing HelloWorld server using the existing channel. */
  public HelloWorldClient(Channel channel) {
    // 'channel' here is a Channel, not a ManagedChannel, so it is not this code's responsibility to
    // shut it down.

    // Passing Channels to code makes code easier to test and makes it easier to reuse Channels.
    blockingStub = GreeterGrpc.newBlockingStub(channel);
    asyncStub = GreeterGrpc.newStub(channel);
  }

  /** Say hello to server. */
  public void greet(String name, String type) throws InterruptedException{
      // logger.info("Will try to greet " + name + " ...");
      try {
        if(type.equals("smallmessage")) {
          HelloRequest request = HelloRequest.newBuilder().setName(name).build();
          HelloReply response;
          for(int i = 0;i < 30;i++) {
            long start_time = System.currentTimeMillis();
            response = blockingStub.sayHello(request);
            long end_time = System.currentTimeMillis();
            System.out.println(end_time - start_time);
          }
        }
        else if(type.equals("clientstreaming")) {
          final CountDownLatch finishLatch = new CountDownLatch(1);

          StreamObserver<HelloReply> responseObserver = new StreamObserver<HelloReply>(){
            @Override
            public void onNext(HelloReply reply) {
              System.out.println("got response for streaming request!");
            }

            @Override
            public void onError(Throwable t) {
              logger.log(Level.WARNING, "error in responseObserver");
              finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
              finishLatch.countDown();
            }
          };

          StreamObserver<HelloRequest> requestObserver = asyncStub.sayHelloStreamingRequest(responseObserver);

          try {
            char[] data = new char[1048576];
            String i_str = new String(data);

            long start_time = System.currentTimeMillis();
            for(int i = 0;i < 30;i++) {
              requestObserver.onNext(HelloRequest.newBuilder().setName(i_str).build());

              if(finishLatch.getCount() == 0) {
                // RPC completed or errored before we finished sending.
                // Sending further requests won't error, but they will just be thrown away
                return;
              }
            }

            // Mark the end of requests
            requestObserver.onCompleted();

            // Receiving happens asynchronously
            if (!finishLatch.await(1, TimeUnit.MINUTES)) {
              logger.log(Level.WARNING, "recordRoute can not finish within 1 minutes");
            }
            long end_time = System.currentTimeMillis();
            System.out.println(end_time - start_time);
          } catch (RuntimeException e) {
            // Cancel RPC
            requestObserver.onError(e);
            throw e;
          }
        }
        else if(type.equals("serverstreaming")) {
          HelloRequest request = HelloRequest.newBuilder().setName(name).build();
          Iterator<HelloReply> response;
          response = blockingStub.sayHelloStreamingReply(request);
          
          for(int i = 0;i < 30;i++) {
            long start_time = System.currentTimeMillis();
            response.next();
            long end_time = System.currentTimeMillis();

            System.out.println(end_time - start_time);
          }
        }
        else if(type.equals("clientserverstreaming")){

        }
        else {
          System.out.println("invalid option!");
        }
      } catch (StatusRuntimeException e) {
        logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
        return;
      }
  }

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting. The second argument is the target server.
   */
  public static void main(String[] args) throws Exception {
    String user = "world";
    String type = "None";
    // Access a service running on the local machine on port 50051
    String target = "localhost:50051";
    // Allow passing in the user and target strings as command line arguments
    if (args.length > 0) {
      if(args[0].equals("smallmessage")) {
        type = "smallmessage";
      }
      else if(args[0].equals("clientstreaming")) {
        char[] data = new char[1048576];
        user = new String(data);
        type = "clientstreaming";
      }
      else if(args[0].equals("serverstreaming")) {
        user = "smallmessage";
        type = "serverstreaming";
      }
      else if(args[0].equals("clientserverstreaming")) {
        char[] data = new char[1048576];
        user = new String(data);
        type = "clientserverstreaming";
      }
    }
    if (args.length > 1) {
      target = args[1];
    }

    // Create a communication channel to the server, known as a Channel. Channels are thread-safe
    // and reusable. It is common to create channels at the beginning of your application and reuse
    // them until the application shuts down.
    ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext()
        .build();
    try {
      HelloWorldClient client = new HelloWorldClient(channel);
      client.greet(user, type);
    } finally {
      // ManagedChannels use resources like threads and TCP connections. To prevent leaking these
      // resources the channel should be shut down when it will no longer be used. If it may be used
      // again leave it running.
      channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
    }
  }
}
