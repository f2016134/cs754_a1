all: client server

client: client.cpp tcp.h udp.h
	g++ client.cpp -I./ -g -o client

server: server.cpp tcp.h udp.h
	g++ server.cpp -I./ -g -o server

clean:
	rm client server
