#pragma once

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>

using namespace std;

//2MB buffer
#define MAXSIZE 1<<21
#define MAXTCPSEND 1024

// Sends [size] bytes of data from [buffer] to socket [fd]
// closes the socket after its done
int tcp_send_and_recv_ack(int fd, void *buffer, size_t size) {
    int ret = 0;
    char ackbuf = 'z';
    struct timeval start_time, end_time;
    size_t data_sent = 0;

    gettimeofday(&start_time, NULL);
    if(size < MAXTCPSEND) {
        if((ret = send(fd, buffer, size, 0)) < 0) {
            perror("send()");
        }
    }
    else {
        while(size > 0) {
            if((ret = send(fd, buffer, size, 0)) < 0) {
                perror("send()");
            }
            size -= ret;
        }
    }
    // cout << "ret: " << ret << "size: " << size << endl;

    if((ret = recv(fd, &ackbuf, sizeof(char), 0)) > 0) {
        // cout << " ACK! " << ackbuf << endl;
    }
    else perror("recv()");
    gettimeofday(&end_time, NULL);

    close(fd);
    return (((end_time.tv_sec - start_time.tv_sec) * 1000000) + 
            (end_time.tv_usec - start_time.tv_usec))/1000;
}

void tcp_recv_and_send_ack(int fd, size_t size) {
    int ret = 0;
    char ack = 't';
    void *recvbuf = (void*)malloc(MAXSIZE*sizeof(char));

    while(size > 0) {
        if((ret = recv(fd, recvbuf, size, 0)) > 0) {
            cout << ret << endl;
        }
        else {
            perror("recv()");
        }
        size -= ret;
    }

    //send ACK
    if((ret = send(fd, &ack, sizeof(char), 0)) < 0) {
        perror("tcp_recv_and_send_ack()");
    }

    free(recvbuf);
    close(fd);
}

// Sends [size] bytes of data from [buffer] to socket [fd]
// closes the socket after its done
int tcp_send_and_recv_ack_nonew(int fd, void *buffer, size_t size) {
    int ret = 0;
    char ackbuf = 'z';
    struct timeval start_time, end_time;

    gettimeofday(&start_time, NULL);
    if((ret = send(fd, buffer, size, 0)) < size/2) {
        perror("send()");
    }

    if((ret = recv(fd, &ackbuf, sizeof(char), 0)) > 0) {
        // cout << " ACK! " << ackbuf << endl;
    }
    else perror("recv()");
    gettimeofday(&end_time, NULL);

    // close(fd);
    return (((end_time.tv_sec - start_time.tv_sec) * 1000000) + 
            (end_time.tv_usec - start_time.tv_usec))/1000;
}

void tcp_recv_and_send_ack_nonew(int fd, size_t size) {
    int ret = 0;
    char ack = 't';
    void *recvbuf = (void*)malloc(MAXSIZE*sizeof(char));

    if((ret = recv(fd, recvbuf, size, 0)) > 0) {
        // for(int i = 0;i < 26;i++) {
        //     cout << ((char*)recvbuf)[i];
        // }
        // cout << endl;
        cout << ret << endl;
    }
    else {
        perror("recv()");
    }

    //send ACK
    if((ret = send(fd, &ack, sizeof(char), 0)) < 0) {
        perror("tcp_recv_and_send_ack()");
    }

    free(recvbuf);
    // close(fd);
}