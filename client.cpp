// basic code for client and server referenced from:
// https://www.geeksforgeeks.org/socket-programming-cc/
// https://beej.us/guide/bgnet/

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "tcp.h"
#include "udp.h"

using namespace std;

#define PORT 8999
#define PROTOCOL 0

int main(int argc, char** argv) {
    int clientfd_tcp = 0, clientfd_udp = 0, ret = 1;
    struct sockaddr_in sa;
    char one_byte_buffer = 'a';
    char *one_mb_buffer = (char*)malloc((1<<20)*sizeof(char));
    memset(one_mb_buffer, 'A', 1<<20);

    //*****************************************TCP TIME MEASUREMENT*******************************************//
    if(strcmp(argv[1], "tcp1B") == 0) {

        for(int i = 0;i < 30;i++) {
            if((clientfd_tcp = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                cout << "socket() error" << endl;
                close(clientfd_tcp);
            }

            sa.sin_family = AF_INET;
            sa.sin_port = htons(PORT);

            ret = inet_pton(AF_INET, argv[2], (void*)&sa.sin_addr);

            if(ret <= 0) {
                perror("inet_pton()");
                close(clientfd_tcp);
            }

            if((ret = connect(clientfd_tcp, (sockaddr*)&sa, sizeof(sa))) < 0) {
                perror("connect()");
                close(clientfd_tcp);
            }

            cout << tcp_send_and_recv_ack(clientfd_tcp, &one_byte_buffer, sizeof(one_byte_buffer)) << endl;
            close(clientfd_tcp);
        }
    //*****************************************END*******************************************//
    }
    else if(strcmp(argv[1], "tcp1MB") == 0) {

        for(int i = 0;i < 30;i++) {
            if((clientfd_tcp = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                cout << "socket() error" << endl;
                close(clientfd_tcp);
            }

            sa.sin_family = AF_INET;
            sa.sin_port = htons(PORT);

            ret = inet_pton(AF_INET, argv[2], (void*)&sa.sin_addr);

            if(ret <= 0) {
                perror("inet_pton()");
                close(clientfd_tcp);
            }

            if((ret = connect(clientfd_tcp, (sockaddr*)&sa, sizeof(sa))) < 0) {
                perror("connect()");
                close(clientfd_tcp);
            }
            unsigned long sz = 1<<20;
            cout << tcp_send_and_recv_ack(clientfd_tcp, one_mb_buffer, sz) << endl;
            close(clientfd_tcp);
        }
    }
    else if(strcmp(argv[1], "udp") == 0) {
    //*****************************************UDP TIME MEASUREMENT*******************************************//
        for(int i = 0;i < 30;i++) {
            if((clientfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
                cout << "socket() error" << endl;
                close(clientfd_udp);
            }

            sa.sin_family = AF_INET;
            sa.sin_port = htons(PORT);

            ret = inet_pton(AF_INET, argv[2], (void*)&sa.sin_addr);

            if(ret <= 0) {
                perror("inet_pton()");
                close(clientfd_udp);
            }

            // if((ret = connect(clientfd_udp, (sockaddr*)&sa, sizeof(sa))) < 0) {
            //     perror("connect()");
            //     close(clientfd_udp);
            // }

            cout << udp_send_and_recv_ack(clientfd_udp, &one_byte_buffer, sizeof(one_byte_buffer), (sockaddr*)&sa, sizeof(sa)) << endl;
            close(clientfd_udp);
        }
    //*****************************************END*******************************************//
    }
    else if(strcmp(argv[1], "tcp1B-conntime-newsocket") == 0) {

        for(int i = 0;i < 100;i++) {
            if((clientfd_tcp = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                cout << "socket() error" << endl;
                close(clientfd_tcp);
            }

            sa.sin_family = AF_INET;
            sa.sin_port = htons(PORT);

            ret = inet_pton(AF_INET, argv[2], (void*)&sa.sin_addr);

            if(ret <= 0) {
                perror("inet_pton()");
                close(clientfd_tcp);
            }

            if((ret = connect(clientfd_tcp, (sockaddr*)&sa, sizeof(sa))) < 0) {
                perror("connect()");
                close(clientfd_tcp);
            }

            cout << tcp_send_and_recv_ack(clientfd_tcp, &one_byte_buffer, sizeof(one_byte_buffer)) << endl;
            close(clientfd_tcp);
        }
    //*****************************************END*******************************************//
    }
    else if(strcmp(argv[1], "tcp1B-conntime-nonew") == 0) {
            if((clientfd_tcp = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                cout << "socket() error" << endl;
                close(clientfd_tcp);
            }

            sa.sin_family = AF_INET;
            sa.sin_port = htons(PORT);

            ret = inet_pton(AF_INET, argv[2], (void*)&sa.sin_addr);

            if(ret <= 0) {
                perror("inet_pton()");
                close(clientfd_tcp);
            }

            if((ret = connect(clientfd_tcp, (sockaddr*)&sa, sizeof(sa))) < 0) {
                perror("connect()");
                close(clientfd_tcp);
            }

            for(int i = 0;i < 100;i++)
                cout << tcp_send_and_recv_ack_nonew(clientfd_tcp, &one_byte_buffer, sizeof(one_byte_buffer)) << endl;
            close(clientfd_tcp);
    //*****************************************END*******************************************//
    }
    else {
        cout << "invalid argument" << endl;
    }

    return 0;
}
