// basic code for client and server referenced from:
// https://www.geeksforgeeks.org/socket-programming-cc/
// https://www.geeksforgeeks.org/udp-server-client-implementation-c/
// https://beej.us/guide/bgnet/

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "tcp.h"
#include "udp.h"

using namespace std;

#define PORT 8999
#define PROTOCOL 0

int main(int argc, char **argv) {
    int serverfd = 0, ret = 1, yes = 1, connfd = 0;
    char buffer;
    struct sockaddr_in sa, client_addr;
    socklen_t client_addr_len;

    if(strcmp(argv[1], "tcp1B") == 0) {
    //*****************************************TCP*******************************************//

        if((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket()");
        }

        if((ret = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))) < 0) {
            perror("setsockopt()");
        }

        sa.sin_family = AF_INET;
        sa.sin_addr.s_addr = INADDR_ANY;
        sa.sin_port = htons(PORT);

        if((ret = bind(serverfd, (sockaddr*)&sa, sizeof(sa))) < 0) {
            perror("bind()");
            close(serverfd);
        }

        if((ret = listen(serverfd, 20)) < 0) {
            perror("listen()");
            close(serverfd);
        }

        for(int i = 0;i < 30;i++) {
            if((connfd = accept(serverfd, (sockaddr *)&client_addr, &client_addr_len)) < 0) {
                perror("accept()");
                close(serverfd);
            }

            tcp_recv_and_send_ack(connfd, sizeof(char));

            close(connfd);
        }

        close(serverfd);
    }
    else if(strcmp(argv[1], "tcp1MB") == 0) {
    //*****************************************TCP*******************************************//

        if((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket()");
        }

        if((ret = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))) < 0) {
            perror("setsockopt()");
        }

        sa.sin_family = AF_INET;
        sa.sin_addr.s_addr = INADDR_ANY;
        sa.sin_port = htons(PORT);

        if((ret = bind(serverfd, (sockaddr*)&sa, sizeof(sa))) < 0) {
            perror("bind()");
            close(serverfd);
        }

        if((ret = listen(serverfd, 20)) < 0) {
            perror("listen()");
            close(serverfd);
        }

        for(int i = 0;i < 30;i++) {
            if((connfd = accept(serverfd, (sockaddr *)&client_addr, &client_addr_len)) < 0) {
                perror("accept()");
                close(serverfd);
            }

            tcp_recv_and_send_ack(connfd, (1<<20));

            close(connfd);
        }

        close(serverfd);
    }
    //*****************************************END*******************************************//

    //*****************************************UDP*******************************************//
    else if(strcmp(argv[1], "udp") == 0) {
            for(int i = 0;i < 100;i++) {
                if((serverfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
                    perror("socket()");
                }

                if((ret = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))) < 0) {
                    perror("setsockopt()");
                }

                sa.sin_family = AF_INET;
                sa.sin_addr.s_addr = INADDR_ANY;
                sa.sin_port = htons(PORT);

                if((ret = bind(serverfd, (sockaddr*)&sa, sizeof(sa))) < 0) {
                    perror("bind()");
                    close(serverfd);
                }

                udp_recv_and_send_ack(serverfd, sizeof(char), (sockaddr*)&client_addr);

                close(serverfd);
        //*****************************************END*******************************************//
        }
    }
    else if(strcmp(argv[1], "tcp1B-conntime-newsocket") == 0) {
    //*****************************************TCP*******************************************//

        if((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket()");
        }

        if((ret = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))) < 0) {
            perror("setsockopt()");
        }

        sa.sin_family = AF_INET;
        sa.sin_addr.s_addr = INADDR_ANY;
        sa.sin_port = htons(PORT);

        if((ret = bind(serverfd, (sockaddr*)&sa, sizeof(sa))) < 0) {
            perror("bind()");
            close(serverfd);
        }

        if((ret = listen(serverfd, 20)) < 0) {
            perror("listen()");
            close(serverfd);
        }

        for(int i = 0;i < 100;i++) {
            if((connfd = accept(serverfd, (sockaddr *)&client_addr, &client_addr_len)) < 0) {
                perror("accept()");
                close(serverfd);
            }

            tcp_recv_and_send_ack(connfd, sizeof(char));

            close(connfd);
        }

        close(serverfd);
    }
    else if(strcmp(argv[1], "tcp1B-conntime-nonew") == 0) {
    //*****************************************TCP*******************************************//

        if((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("socket()");
        }

        if((ret = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes))) < 0) {
            perror("setsockopt()");
        }

        sa.sin_family = AF_INET;
        sa.sin_addr.s_addr = INADDR_ANY;
        sa.sin_port = htons(PORT);

        if((ret = bind(serverfd, (sockaddr*)&sa, sizeof(sa))) < 0) {
            perror("bind()");
            close(serverfd);
        }

        if((ret = listen(serverfd, 20)) < 0) {
            perror("listen()");
            close(serverfd);
        }

        if((connfd = accept(serverfd, (sockaddr *)&client_addr, &client_addr_len)) < 0) {
            perror("accept()");
            close(serverfd);
        }

        for(int i = 0;i < 100;i++)
            tcp_recv_and_send_ack_nonew(connfd, sizeof(char));

        close(connfd);

        close(serverfd);
    }
    return 0;
}