#pragma once

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>

using namespace std;

//2MB buffer
#define MAXSIZE 1<<21

// Sends [size] bytes of data from [buffer] to socket [fd]
// closes the socket after its done
int udp_send_and_recv_ack(int fd, void *buffer, size_t size, sockaddr *saddr, socklen_t slen) {
    int ret = 0;
    char ackbuf = 'z';
    socklen_t serveraddrlen = sizeof(saddr);
    struct timeval start_time, end_time;

    gettimeofday(&start_time, NULL);
    if((ret = sendto(fd, buffer, size, 0, saddr, slen)) != size) {
        perror("send()");
    }

    if((ret = recvfrom(fd, &ackbuf, sizeof(char), 0, saddr, &serveraddrlen)) > 0) {
        // cout << ret << " ACK! " << ackbuf << endl;
    }
    else perror("recv()");
    gettimeofday(&end_time, NULL);

    close(fd);
    return (((end_time.tv_sec - start_time.tv_sec) * 1000000) + 
            (end_time.tv_usec - start_time.tv_usec))/1000;
}

void udp_recv_and_send_ack(int fd, size_t size, sockaddr *caddr) {
    int ret = 0;
    char ack = 'u';
    void *recvbuf = (void*)malloc(MAXSIZE*sizeof(char));
    socklen_t caddrlen = sizeof(*caddr);

    if((ret = recvfrom(fd, recvbuf, size, 0, caddr, &caddrlen)) > 0) {
        for(int i = 0;i < 26;i++) {
            cout << ((char*)recvbuf)[i];
        }
        cout << endl;
    }
    else {
        perror("recvfrom()");
    }

    //send ACK
    if((ret = sendto(fd, &ack, sizeof(char), 0, caddr, caddrlen)) < 0) {
        perror("udp_recv_and_send_ack()");
    }
    close(fd);
}